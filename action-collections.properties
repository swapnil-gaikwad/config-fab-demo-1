# -------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# -------------------------------------------------------------------------------------------

#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false

# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-collections

# Port 51002
server.port=51002

# Module Abbreviation Name (preferably <= 3 characters and only Upper Case Alphabets A-Z)
# e.g. LMS (for Liquidity), IPSH (for Payments), Collections (for Collections & Receivables)...
# This is used for deriving:
# 	- ElasticSearch Index name as <moduleAbbr>-requests (e.g. lms-requests, ipsh-requests, cnr-requests)
#	- Deriving Destination names in Release Trigger and State Update handler modules
ModuleAbbr=COLL

# Header key and value, to be used as security token while calling APIs for this module
iGTBD-AtomicAPI-SharedKey=I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq

# ES Database connection details
#ES.DB.Host=localhost
#ES.DB.Port=443
#ES.DB.User=
#ES.DB.Password=

Redis.DB.Url=10.233.250.122:6379
Redis.DB.Password=

# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

# Camel Messages to be logged at this level (Possible values: ERROR, WARN, INFO, DEBUG, TRACE, OFF)
CamelMessageLoggingLevel=DEBUG

# Release Batch Size
# It is max number of requests to be released per batch (Approved, Retry batch)
ReleaseBatchSize=200

# JMS Broker which is to be used by Commons for handling Release Batch Trigger requests, State Update requests
# Supported values (ActiveMQ / RabbitMQ) - default is ActiveMQ
# This is used for dynamically identifying destination names for subscribing to/publishing Release Batch or State Update events
#JmsBroker=ActiveMQ


INSTANCE.1.Http.ServiceUrl=http://localhost:51002

Backend.ValidateServiceUri=http://103.231.209.251:10004
Backend.ReleaseServiceUri=t3://103.231.209.251:10004

#Backend.ValidateServiceUri=http://localhost:8082
#Backend.ReleaseServiceUri=t3://10.10.4.58:10003



#############################################################################################
###### Purge Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Purge Batch Trigger events 
PurgeTriggerHandler.CamelComponent=rabbitmq
PurgeTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Minimum number of days the data must be retained in State Store (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.MinRetentionDays=720
# Flag indicating whether purging of IN_PROGRESS requests from State Store is allowed or not (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.PurgeOfInProgressAllowed=false


# Comma separated list of Payload Types, for which this module need to support running Purge Batches
PurgeTriggerHandler.PayloadTypes=Mandate,DirectDebitTransactions,VirtualAccountHirearchy

# Used as sleep time (in milliseconds) between each message retry, when msg is requeue'd.
# Keep it commented, unless need to override default value.
#MsgRetrySleepTime=250

# Used as max time (in seconds) a message to be retried for in requeue mode
# Keep it commented, unless need to override default value.
#MsgRetryMaxTime=86400

##################################################################
### Zipkin specific options
##################################################################
spring.zipkin.enabled=false
spring.sleuth.enabled=false
#spring.sleuth.web.additionalSkipPattern=.*/getDetails|.*/updateDetails
#spring.sleuth.sampler.probability=1.0
#spring.zipkin.baseUrl=http://localhost:9411

#############################################################################################
###### Release Retry Configuration
#############################################################################################
RelRetryCfgIdentifierKeys=MANDATE_ANY,DIRECT_DEBIT_TRANSACTIONS_ANY,VIRTUAL_ACCOUNT_ANY

###
# Release Retry Config Identifier key based configuration
# 
# RelRetryCfgIdentifier.<identifierKey>.payloadType=
# RelRetryCfgIdentifier.<identifierKey>.requestType=
# RelRetryCfgIdentifier.<identifierKey>.maxReleaseRetry=
# Where,
#	<payloadType,requestType> together forms a unique key combination for providing related configuration 
#	payloadType = type of the payload e.g. SweepStructure, LoanAgreement or any
#	requestType = type of the request e.g. create, update, delete or any
# 	maxReleaseRetry = to indicate how many times this message should be retried for releasing before it is marked as Failed
#			(field is optional - default value will be applied)
#
# Configuration is applied in following order of priority, for a combination of <payloadType> and <requestType>:
# 1. <payloadType> and <requestType> exactly match with values provided here.
# 2. Else - <payloadType> and "any"
# 3. Else - "any" and <requestType>
# 4. Else - "any" and "any"
# 5. Else - default retry configuration is applied
#
###

RelRetryCfgIdentifier.MANDATE_ANY.payloadType=Mandate
RelRetryCfgIdentifier.MANDATE_ANY.requestType=any
RelRetryCfgIdentifier.MANDATE_ANY.maxReleaseRetry=

RelRetryCfgIdentifier.DIRECT_DEBIT_TRANSACTIONS_ANY.payloadType=DirectDebitTransactions
RelRetryCfgIdentifier.DIRECT_DEBIT_TRANSACTIONS_ANY.requestType=any
RelRetryCfgIdentifier.DIRECT_DEBIT_TRANSACTIONS_ANY.maxReleaseRetry=

#RelRetryCfgIdentifier.VIRTUAL_ACCOUNT_UPDATE.payloadType=VirtualAccountHirearchy
#RelRetryCfgIdentifier.VIRTUAL_ACCOUNT_UPDATE.requestType=update
#RelRetryCfgIdentifier.VIRTUAL_ACCOUNT_UPDATE.maxReleaseRetry=5

RelRetryCfgIdentifier.VIRTUAL_ACCOUNT_ANY.payloadType=VirtualAccountHirearchy
RelRetryCfgIdentifier.VIRTUAL_ACCOUNT_ANY.requestType=any
RelRetryCfgIdentifier.VIRTUAL_ACCOUNT_ANY.maxReleaseRetry=



##################################################################
### Release Connector Specific Configuration
##################################################################
#RelConnector.HTTP_CONNECTOR.route=direct:HttpConnectorRoute

RelConnector.JMS_CONNECTOR.route=direct:JmsConnectorRoute


### Transformer routes
# Mandate specific
RelConnector.MANDATE_TXFMR.route=direct:TransformMandateRoute

# Direct Debit Transactions specific
RelConnector.DIRECT_DEBIT_TRANSACTIONS_TXFMR.route=direct:TransformDirectDebitTransactionsRoute


# Virtual Account specific
RelConnector.VIRTUAL_ACCOUNT_TXFMR.route=direct:TransformVirtualAccountRoute


### Backend Endpoints
# Mandate specific
# Endpoint using JMS

# Endpoint using HTTP
RelConnector.MANDATE_JMS.endpoint=INSTANCE.1.WLS.Component:queue:COM_INT_QUEUE?disableReplyTo=true
#RelConnector.MANDATE_JMS.endpoint=direct:BackendReleaseStub

# Direct Debit Transactions specific stub
RelConnector.DIRECT_DEBIT_TRANSACTIONS_JMS.endpoint=INSTANCE.1.WLS.Component:queue:COM_INT_QUEUE?disableReplyTo=true
#RelConnector.DIRECT_DEBIT_TRANSACTIONS_JMS.endpoint=direct:BackendReleaseStub


# Virtual Account specific stub
RelConnector.VIRTUAL_ACCOUNT_JMS.endpoint=INSTANCE.1.WLS.Component:queue:COM_INT_QUEUE?disableReplyTo=true
#RelConnector.VIRTUAL_ACCOUNT_JMS.endpoint=direct:BackendReleaseStub


### Routing slips
#RelConnector.MandateCreate.routingSlip= ${RelConnector.MANDATE_TXFMR.route},\
#		${RelConnector.JMS_CONNECTOR.route},\
#		${RelConnector.MANDATE_JMS.endpoint}
RelConnector.MandateAny.routingSlip=${RelConnector.MANDATE_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.MANDATE_JMS.endpoint}

RelConnector.DirectDebitTransactionsAny.routingSlip=${RelConnector.DIRECT_DEBIT_TRANSACTIONS_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.DIRECT_DEBIT_TRANSACTIONS_JMS.endpoint}

RelConnector.VirtualAccountAny.routingSlip=${RelConnector.VIRTUAL_ACCOUNT_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.VIRTUAL_ACCOUNT_JMS.endpoint}
		
##################################################################
### Validation specific Connector routes
##################################################################
ValConnector.HTTP_CONNECTOR.route=direct:HttpConnectorValidateRoute
ValConnector.HTTP_CONNECTOR.route.CREATE=direct:HttpConnectorValidateRouteCreate
ValConnector.HTTP_CONNECTOR.route.UPDATE=direct:HttpConnectorValidateRouteUpdate
ValConnector.HTTP_CONNECTOR.route.DELETE=direct:HttpConnectorValidateRouteDelete


### Transformer routes
# Mandate specific  - below commented as we are not changing payload transformation for validation
#ValConnector.MANDATE_TXFMR.route=direct:TransformMandateRoute

#Directm Debit Transaction specific -below commented as we are not changing payload transformation for validation
#ValConnector.DIRECT_DEBIT_TRANSACTIONS_TXFMR.route=direct:TransformDirectDebitTransactionsRoute


### Backend Endpoints
# SweepStructure specific
# Validation Endpoint using Embedded approach
#ValConnector.MANDATE_BEAN.endpoint=bean:validateSweepStructureBean?method=validateResourcePayload
# Validation Endpoint using Interface approach

#Mandate Specific
#ValConnector.MANDATE_HTTP.endpoint=direct:BackendValidateStub
ValConnector.MANDATE_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/cmsweb/igtb-cnr/v1/corporate/mandates

#Direct Debit Transactions specific
#ValConnector.DIRECT_DEBIT_TRANSACTIONS_HTTP.endpoint=direct:BackendValidateStub
ValConnector.DIRECT_DEBIT_TRANSACTIONS_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/cmsweb/igtb-cnr/v1/corporate/directdebits

#Direct Debit Transactions specific
#ValConnector.VIRTUAL_ACCOUNT_HTTP.endpoint=direct:BackendValidateStub
ValConnector.VIRTUAL_ACCOUNT_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/cmsweb/igtb-cnr/v1/corporate/virtual-accounts-hierarchy



### Routing Slips
# Mandate specific  ${ValConnector.MANDATE_TXFMR.route},\
#ValConnector.MandateCreate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route},\
#        ${ValConnector.MANDATE_HTTP.endpoint}
#ValConnector.MandateUpdate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route},\
#        ${ValConnector.MANDATE_HTTP.endpoint}
#ValConnector.MandateDelete.routingSlip= ${ValConnector.HTTP_CONNECTOR.route},\
#		${ValConnector.MANDATE_HTTP.endpoint}
ValConnector.MandateCreate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.CREATE},\
        ${ValConnector.MANDATE_HTTP.endpoint}
ValConnector.MandateUpdate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.UPDATE},\
        ${ValConnector.MANDATE_HTTP.endpoint}
ValConnector.MandateDelete.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.DELETE},\
		${ValConnector.MANDATE_HTTP.endpoint}

ValConnector.DirectDebitTransactionsCreate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.CREATE},\
        ${ValConnector.DIRECT_DEBIT_TRANSACTIONS_HTTP.endpoint}

ValConnector.VirtualAccountCreate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.CREATE},\
        ${ValConnector.VIRTUAL_ACCOUNT_HTTP.endpoint}
ValConnector.VirtualAccountUpdate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.UPDATE},\
        ${ValConnector.VIRTUAL_ACCOUNT_HTTP.endpoint}


        
##################################################################
### Backend Instance specific configuration for Camel Component
##################################################################
# This is used in Spring Camel beans XML
#INSTANCE.1.AMQ.ProviderUrl=tcp://localhost:61616
#Digital.JmsBroker.Type=ActiveMQ
#Digital.JmsBroker.ProviderUrl=tcp://localhost:61616
#Digital.JmsBroker.User=
#Digital.JmsBroker.Password=

# Represents DataCenter region, country the process events/data belongs to
DataCenter.Region=
DataCenter.Country=

# Represents service key regex pattern (product/subproduct), this module is supposed to process events/data related to
Digital.ServiceKey.Patterns=colrec/col.*

#Digital.JmsBroker.Type=RabbitMQ

#Digital.JmsBroker.Host=localhost
#Digital.JmsBroker.Port=61001
#Digital.JmsBroker.User=guest
#Digital.JmsBroker.Password=guest
#Digital.JmsBroker.VHost=/

# Message Broker which is to be used by Commons for Release Batch Triggers, State Update Events and Event Publisher
# Supported value - RabbitMQ
Digital.MsgBroker.Type=RabbitMQ
# This is used by Commons for connecting to RabbitMQ Server
Digital.RabbitMQ.Host=10.233.245.190
Digital.RabbitMQ.Port=5672
Digital.RabbitMQ.User=admin
Digital.RabbitMQ.Password=s3cr3t
Digital.RabbitMQ.VHost=/


# Name of the Apache Camel Component,
# which is to be used by Commons package for publishing Events / Triggers
MsgPubHandler.CamelComponent=rabbitmq

#############################################################################################
###### Release Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel JMS Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Release Batch Trigger events 

#RelTriggerHandler.JmsCamelComponent=rabbitmq://${Digital.JmsBroker.Host}:${Digital.JmsBroker.Port}

#RelTriggerHandler.JmsCamelComponent=INSTANCE.1.AMQ.Component

# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to Release Batch Trigger events 
RelTriggerHandler.CamelComponent=rabbitmq
RelTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30


###### Comma separated list of Release Trigger Handler Identifier Keys (user defined keys)
#RelTriggerHandlerKeys=MANDATE_APPROVED,MANDATE_RETRY,DD_TRANSACTIONS_APPROVED,DD_TRANSACTIONS_RETRY,VIRTUAL_ACCOUNT_APPROVED,VIRTUAL_ACCOUNT_RETRY


###
# Release Trigger Handler key based configuration`
#
# RelTriggerHandler.<handlerKey>.payloadType=
# RelTriggerHandler.<handlerKey>.subStatus=
#
# Where,
#	payloadType = type of the payload, the release batch is to be executed for, on receiving external trigger e.g. SweepStructure, LoanAgreement
#	subStatus = subStatus of the requests in State Store, that are to be attempted for release e.g. APPROVED, RELEASE_RETRY
#
# Release Trigger Handlers are set up based on each <payloadType>, <subStatus> by deriving appropriate:
# 	consumerEndpoint = Camel consumer endpoint which waits for Release Trigger Request
#	okEndpoint = Camel target endpoint, where a message is sent when Release Batch result IS successful
#	nokEndpoint = Camel target endpoint, where a message is sent when Release Batch result IS NOT successful
#
# For example,
#	For
#		JmsBroker defined as "ActiveMQ",
#	Endpoints formats will be:
#		consumerEndpoint = <jmsCamelComponent>:queue:Consumer.AAF_<moduleAbbr>.VirtualTopic.Action.ReleaseTrigger.<payloadType><subStatus>?concurrentConsumers=1
#		okEndpoint = <jmsCamelComponent>:topic:VirtualTopic.Action.ReleaseResult.<payloadType><subStatus>.OK
#		nokEndpoint = <jmsCamelComponent>:topic:VirtualTopic.Action.ReleaseResult.<payloadType><subStatus>.NOK
#	Hence, for
#		payloadType = SweepStructure
#		subStatus = APPROVED
#	sample derived values will be:
#		consumerEndpoint=INSTANCE.1.AMQ.Component:queue:Consumer.AAF_LIQ.VirtualTopic.Action.ReleaseTrigger.SweepStructureApproved?concurrentConsumers=1
#		okEndpoint=INSTANCE.1.AMQ.Component:topic:VirtualTopic.Action.ReleaseResult.SweepStructureApproved.OK
#		nokEndpoint=INSTANCE.1.AMQ.Component:topic:VirtualTopic.Action.ReleaseResult.SweepStructureApproved.NOK
#
###

# Comma separated list of Payload Types, for which this module need to support running Release Batches
RelTriggerHandler.PayloadTypes=Mandate,DirectDebitTransactions,VirtualAccountHirearchy


#################################################################################################
###### State Update Handler specific Configuration
#################################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to B/E State Update events 
StateUpdHandler.CamelComponent=rabbitmq
StateUpdHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30
###### Comma separated list of (B/E) State Update Handler Identifier Keys (user defined keys)
#StateUpdHandlerKeys=MANDATE,DD_TRANSACTIONS,VIRTUAL_ACCOUNT


###
# State Update Handler key based configuration
#
# StateUpdHandler.<handlerKey>.payloadType=
#
# Where,
#	payloadType = type of the payload, the Backend State Updates to be accepted for e.g. SweepStructure, LoanAgreement
#
# State Update Handlers are set up based on each <payloadType> by deriving appropriate:
# 	consumerEndpoint = Camel consumer endpoint which waits for State Update Request from B/E
#	okEndpoint = <not used>
#	nokEndpoint = Camel target endpoint, where a message is sent when State Update result IS NOT successful
#
# For example,
#	For
#		JmsBroker defined as "ActiveMQ",
#	Endpoints formats will be:
#		consumerEndpoint = <jmsCamelComponent>:queue:Consumer.AAF_<moduleAbbr>.VirtualTopic.Action.StateUpdate.<payloadType>?concurrentConsumers=1
#		nokEndpoint = <jmsCamelComponent>:topic:VirtualTopic.Action.StateUpdResult.<payloadType>.NOK
#	Hence, for
#		payloadType = SweepStructure
#	sample derived values will be:
#		consumerEndpoint=INSTANCE.1.AMQ.Component:queue:Consumer.AAF_LIQ.VirtualTopic.Action.StateUpdate.SweepStructure?concurrentConsumers=1
#		nokEndpoint=INSTANCE.1.AMQ.Component:topic:VirtualTopic.Action.StateUpdResult.SweepStructure.NOK
#
###

#StateUpdHandler.MANDATE.payloadType=Mandate
#StateUpdHandler.DD_TRANSACTIONS.payloadType=DirectDebitTransactions
#StateUpdHandler.VIRTUAL_ACCOUNT.payloadType=VirtualAccountHirearchy
#BACKEND WEBLOGIC Initial Connection factory , provider url and JNDI connection factory name settings
INSTANCE.1.WLS.InitialContextFactory=weblogic.jndi.WLInitialContextFactory
#INSTANCE.1.WLS.ProviderUrl=t3://10.10.7.143:20013
INSTANCE.1.WLS.ProviderUrl=${Backend.ReleaseServiceUri}
INSTANCE.1.WLS.JndiConnFactoryName=COM_INT_QCF

StateUpdHandler.wlsCamelComponent=INSTANCE.1.WLS.Component
RelConnector.MANDATE_JMS.outqueue_endpoint=MAND_RESP_QUEUE
